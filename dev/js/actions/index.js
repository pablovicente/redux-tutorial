export const selectUser = (user) => {
  return {
    type: 'USER_SELECTED',
    payload: user
  }
};

export const selectHardware= (hardware) => {
  return {
    type: 'HARDWARE_SELECTED',
    payload: hardware
  }
};