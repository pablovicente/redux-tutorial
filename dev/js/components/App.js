import React from 'react';
import UserList from '../containers/user-list'
import UserDetail from '../containers/user-detail'
import HardwareList from '../containers/hardware-list'
import HardwareDetail from '../containers/hardware-selected'
require('../../scss/style.scss');

const App = () => (
  <div>
    <table>
      <tbody>
      <tr>
        <td>
          <h2>Users List</h2>
          <UserList />
          <hr/>
          <h2>User Details:</h2>
          <UserDetail />
        </td>
        <td>
          <h2>Another Component</h2>
          <HardwareList />
          <hr/>
          <h2>Hardware Details:</h2>
          <HardwareDetail />
        </td>
      </tr>
      </tbody>
    </table>
  </div>
);

export default App;