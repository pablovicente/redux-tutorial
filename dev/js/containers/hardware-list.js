import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {selectHardware} from '../actions/index';

class HardwareList extends Component {
  createListItems(){
    return this.props.hardware.map((aHardware) => <li onClick={ () => this.props.selectHardware(aHardware)} key={aHardware.id}>{aHardware.model}</li>)
  }

  render(){
    return(
      <ul>
        {this.createListItems()}
      </ul>
    )
  }
}

function mapStateToProps(state){
  return {
    hardware: state.hardware
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectHardware:selectHardware}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(HardwareList);