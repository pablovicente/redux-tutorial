import React, {Component} from 'react';
import {connect} from 'react-redux';

class HardwareDetail extends Component {
  render(){
    if(!this.props.selectedHardware){
      return(
        <h3>Select Hardware</h3>
      )
    }
    return(
      <div>
        <h4>Name: {this.props.selectedHardware.make}</h4>
        <h4>Name: {this.props.selectedHardware.model}</h4>
      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    selectedHardware: state.selectedHardware
  }
}

export default connect(mapStateToProps)(HardwareDetail);