import React, {Component} from 'react';
import {connect} from 'react-redux';

class UserDetail extends Component {
  render(){
    if(!this.props.user){
      return(
        <h3>Select a user</h3>
      )
    }
    return(
      <div>
        <h3>Name: {this.props.user.first}</h3>
        <img src={this.props.user.thumbnail} />
      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    user: state.activeUser
  }
}

export default connect(mapStateToProps)(UserDetail);