import {combineReducers} from 'redux';
import userReducer from './reducer-users';
import hardwareReducer from './reducer-hardware';
import ActiveUserReducer from './reducer-active-user';
import SelectedHardware from './reducer-selected-hardware';

const allReducers = combineReducers(
  {
    users: userReducer,
    activeUser: ActiveUserReducer,
    hardware: hardwareReducer,
    selectedHardware: SelectedHardware
  }
);

export default allReducers;