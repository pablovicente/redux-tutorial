export default function(){
  return [
    {
      id: 1,
      make: 'Esp',
      model: 32,
      description: 'µcu'
    },
    {
      id: 2,
      make: 'Rpi',
      model: '3B',
      description: 'SOC'
    },
    {
      id: 3,
      make: 'Digispark',
      model: 'At85',
      description: 'Tiny µcu'
    }
  ];
}